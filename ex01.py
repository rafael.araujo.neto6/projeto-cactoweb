x = []
valores = []
menor = 0


for c in range(1, 7):
    x.append(int(input(f'Digite o valor da {c}° posição: '))) # adiciona os valores no final da lista X

    for val in x: # faz com que os valores de da lista que são str virem int
        valores.append(int(val))  # adiciona os valores transformados na lista valores

    if val < 10 and val >= 0: # faz a verificação dos valores se estão entre 0 e 9
        print('Valor adicionado! \n') # da um feedback visual ao usuário, mostrando que o valor foi adicionado

    else:
        print('Por favor digite um valor válido') # mostra ao usuário que valor digitado é inválido
        x.pop() # remove o último item, que é o valor inválido
        x.append(int(input(f'Digite o valor na posição {c}°: '))) # pede novamente ao usuário para digitar um valor válido
print(f'Os valores adicionados foram: \n{x}\n')
print(f'O menor valor digitado foi {min(x)}.') # chama a função e imprime o menor valor

