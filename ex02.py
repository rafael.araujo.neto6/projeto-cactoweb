from operator import itemgetter

temp = []
princ = []

print('Digite as notas dos alunos')
while True:
    temp.append(str(input('Nome: ')))
    temp.append(float(input('Nota: ')))
    princ.append(temp[:]) # faz uma cópia da lista

    temp.clear() # faz a limpesa da lista para não haver valores repetidos

    resp = str(input('Quer continuar? [S/N] ')) # variável de controle para saber se o usuário deseja continuar ou não
    if resp in 'nN':
        break


nota = (sorted(princ, key=itemgetter(1), reverse=True)) # faz a ordenação da tabela princ pelo segundo item que é nota, e de forma inversa, para ficar no maior para o menor
print(nota[0], nota[1], nota[2])

